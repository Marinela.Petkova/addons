create or replace table addon_images
(
    image_id int auto_increment
        primary key,
    name     varchar(2000) not null,
    type     varchar(50)   null,
    image    mediumblob    null
);

create or replace table addons_binary
(
    addon_id     int      not null
        primary key,
    addon_binary longblob not null
);

create or replace table collection_of_addons
(
    user_id  int not null,
    addon_id int not null
);

create or replace table images
(
    id    int auto_increment
        primary key,
    name  varchar(200) not null,
    image longblob     null,
    type  varchar(50)  null
);

create or replace table tags
(
    id  int auto_increment
        primary key,
    tag varchar(10) null
);

create or replace table target_ides
(
    id          int auto_increment
        primary key,
    target_name varchar(30) not null
);

create or replace table users
(
    id           int auto_increment
        primary key,
    username     varchar(20) not null,
    password     varchar(20) not null,
    email        varchar(30) not null,
    phone_number varchar(10) null,
    photo        int         not null,
    date         datetime    not null,
    first_name   varchar(20) not null,
    last_name    varchar(20) not null,
    is_admin     tinyint(1)  not null,
    constraint users_images_null_fk
        foreign key (photo) references images (id)
);

create or replace table addons
(
    id             int auto_increment
        primary key,
    creator        int         not null,
    addon_name     varchar(20) not null,
    description    text        not null,
    target_id      int         null,
    binary_content varchar(30) not null,
    origin         text        null,
    addon_image    int         not null,
    constraint addons_target_ide_null_fk
        foreign key (target_id) references target_ides (id),
    constraint adons_addon_images_null_fk
        foreign key (addon_image) references addon_images (image_id),
    constraint `create`
        foreign key (creator) references users (id)
);

create or replace table addons_tags
(
    addon_id int not null,
    tag_id   int null,
    constraint addons_tags___fk
        foreign key (addon_id) references addons (id),
    constraint addons_tags_tags_null_fk
        foreign key (tag_id) references tags (id)
);

