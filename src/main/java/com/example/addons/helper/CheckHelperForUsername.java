package com.example.addons.helper;

import com.example.addons.exceptions.EntityNotFoundException;
import com.example.addons.models.User;
import com.example.addons.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.security.Principal;

@Component
@RequiredArgsConstructor
public class CheckHelperForUsername {

    UserRepository repository;

    public User getUser(Principal principal) {
        try {
            repository.findByUsername(principal.getName());
        } catch (EntityNotFoundException e) {
            e.getMessage();
        }
        return repository.findByUsername(principal.getName());
    }

}
