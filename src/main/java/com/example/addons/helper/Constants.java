package com.example.addons.helper;

public final class Constants {

    public Constants() {
    }

    public static final class ValidationConstants {

        public static final int MIN_NAME_LENGTH = 2;
        public static final int MAX_NAME_LENGTH = 20;
        public static final int MIN_ADDON_NAME_LENGTH = 3;
        public static final int MAX_ADDON_NAME_LENGTH = 30;
        public static final int MIN_DESCRIPTION_LENGTH = 5;
        public static final int MAX_DESCRIPTION_LENGTH = 50;
        public static final int MIN_TAG_NAME_LENGTH = 5;
        public static final int MAX_TAG_NAME_LENGTH = 50;
        public static final int MIN_BINARY_CONTENT_LENGTH = 3;
        public static final int MAX_BINARY_CONTENT_LENGTH = 30;
        public static final int MIN_ORIGIN_LENGTH = 2;
        public static final int MAX_ORIGIN_LENGTH = 500;

        public static final String ERROR = "error";
        public static final String BINDING_RESULT_ERRORS = "errors";
        public static final String REGISTER_FIRST = "registerFirst";
        public static final String THIS_FIELD_CANNOT_BE_BLANK = "This field cannot be blank!";
        public static final String EMAIL_REGEX = "^(.+)@(.+)$";
        public static final String PLEASE_PROVIDE_MATCHING_PASSWORDS = "Please provide matching passwords!";
        public static final String CANT_BE_EMPTY = " can't be empty!";
        public static final String PHONE_NUMBER_LENGTH = "Phone number should contain 10 digits.";
        public static final String PASSWORD_VALIDATION = "Password must contain at least 1 uppercase," +
                " 1 lowercase, 1 special character and 1 digit ";
        public static final String PASSWORD_REGEX = "^((?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])){4,12}$";


    }

    public static final class UserConstants {
        public static final String CREATE_USER_DTO = "createUserDto";
        public static final int DEFAULT_PHOTO_ID = 1;

    }

    public static final class QueryConstants {

        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String EMAIL = "email";

        public static final String TELEPHONE = "telephone";
        public static final String USERNAME = "username";
        public static final String PHOTO = "image";
        public static final String TARGET = "target";
        public static final String ADDON = "addon";
        public static final String DESCRIPTION = "description";
        public static final String ORIGIN = "origin";
        public static final String BINARY_CONTENT = "binary content";
    }

    public static final class Views {
        public static final String EMAIL_CONFIRMATION_VIEW = "confirmation/email-confirmation";
        public static final String REGISTER_VIEW = "user/register";
    }

    public static final class ErrorMessages {
        public static final String FILE_HAS_NOT_BEEN_SAVED_S = "File has not been saved: %s";
        public static final String IMAGE_ERROR_MESSAGE = "Profile image must not be empty and less than 1 МB in size.";
        public static final String DUPLICATE_EMAIL_ERROR_MESSAGE = "User with this email already exist.";
        public static final String DUPLICATE_USERNAME_ERROR_MESSAGE = "User with this username already exist";
        public static final String DUPLICATE_PHONE_NUMBER_ERROR_MESSAGE = "User with this phone number already exist";
        public static final String ACTIVATION_CODE_NOT_ACTIVE = "Code not active. Maybe user is activated already?";
        public static final String ACTIVATION_CODE_IS_EXPIRED = "Code expired send new code";
        public static final String YOU_ARE_NOT_AUTHORIZED = "Only admin or owner can modify the profile";
        public static final String USER_NOT_FOUND = "There is not an active user with the search criteria";
        public static final String ERROR_MESSAGE_TAG_NAME_LENGTH = "Tag name should be between 5 and 50 symbols";

    }

    public static class Rest {


        public static final String USER_IS_ALREADY_BLOCKED = "User is already blocked!";
        public static final String USER_IS_NOT_BLOCKED = "User is not blocked!";
        public static final String ADMINS_CAN_NOT_BE_BLOCKED = "Admins can not be blocked!";
        public static final String USER_IS_ALREADY_ADMIN = "User is already an admin!";
        public static final String NOT_ALLOWED_OPERATION = "User can not be promoted to admin! Check user details!";
        public static final String FILE_RECEIVED_SUCCESSFULLY = "Image was received successfully!";
        public static final String FILE_DELETED_SUCCESSFULLY = "Image was deleted!";
        public static final String CAN_NOT_BE_DELETED = "Default image can not be deleted!";


    }

}
