package com.example.addons.validations.impl;

public interface Passwordable {
    String getPassword();

    String getConfirmationPassword();
}
