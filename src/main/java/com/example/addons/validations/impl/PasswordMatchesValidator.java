package com.example.addons.validations.impl;

import com.example.addons.validations.PasswordMatches;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Passwordable> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(final Passwordable passwordable, final ConstraintValidatorContext context) {
        if (passwordable == null) return false;
        return passwordable.getPassword().equals(passwordable.getConfirmationPassword());
    }


}
