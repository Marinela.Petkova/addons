package com.example.addons.services;

import com.example.addons.exceptions.DuplicateEntityException;
import com.example.addons.exceptions.EntityNotFoundException;
import com.example.addons.models.Target;
import com.example.addons.repositories.TargetRepository;
import com.example.addons.services.contracts.TargetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.addons.helper.Constants.QueryConstants.TARGET;

@Service
public class TargetServiceImpl implements TargetService {

    private final TargetRepository targetRepository;

    @Autowired
    public TargetServiceImpl(TargetRepository targetRepository) {
        this.targetRepository = targetRepository;
    }

    @Override
    public List<Target> getAll() {
        return targetRepository.findAll();
    }

    @Override
    public Target getById(int id) {
        return targetRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(TARGET, id));
    }

    @Override
    public Target getByName(String name) {
        return targetRepository.findByTargetName(name);
    }

    @Override
    public void create(Target target) {
        boolean targetExists = true;

        try {
            targetRepository.findByTargetName(target.getTargetName());
        } catch (EntityNotFoundException e) {
            targetExists = false;
        }

        if (targetExists) {
            throw new DuplicateEntityException("Target", "name", target.getTargetName());
        }
        targetRepository.save(target);
    }
}
