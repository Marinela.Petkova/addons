package com.example.addons.services;

import com.example.addons.exceptions.FileStorageException;
import com.example.addons.helper.ImageUtility;
import com.example.addons.models.Addon;
import com.example.addons.models.AddonImage;
import com.example.addons.repositories.AddonImageRepository;
import com.example.addons.services.contracts.AddonImageService;
import com.example.addons.services.contracts.AddonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.example.addons.helper.Constants.ErrorMessages.IMAGE_ERROR_MESSAGE;

@Service
public class AddonImageServiceImpl implements AddonImageService {
    private final AddonImageRepository addonImageRepository;
    private final AddonService addonService;

    @Autowired
    public AddonImageServiceImpl(AddonImageRepository addonImageRepository,
                                 AddonService addonService) {
        this.addonImageRepository = addonImageRepository;
        this.addonService = addonService;
    }

    @Override
    public AddonImage getFile(int imageId) {
        return addonImageRepository.getImageById(imageId);
    }

    @Transactional
    @Override
    public int saveFile(MultipartFile file) {
        try {
            AddonImage image = new AddonImage(file.getOriginalFilename(),
                    file.getContentType(), ImageUtility.compressImage(file.getBytes()));

            addonImageRepository.save(image);
            return image.getId();
        } catch (Exception e) {
            throw new FileStorageException(e.getMessage(), e);
        }
    }

    @Override
    public void update(MultipartFile file, int addonId) {
        if (file.isEmpty() || file.getSize() > 1000000) {
            throw new FileStorageException(IMAGE_ERROR_MESSAGE);
        }

        try {
            Addon addon = addonService.getById(addonId);
            AddonImage oldImage = addon.getAddonImage();

            if (oldImage.getId() == 1) {

                int id = saveFile(file);
                addon.setAddonImage(getFile(id));
                addonService.update(addon);
            } else {
                oldImage.setName(file.getOriginalFilename());
                oldImage.setType(file.getContentType());
                oldImage.setImage(ImageUtility.compressImage(file.getBytes()));
                addonImageRepository.saveAndFlush(oldImage);
            }
        } catch (IOException e) {
            throw new FileStorageException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(AddonImage image) {
        addonImageRepository.delete(image);
    }
}
