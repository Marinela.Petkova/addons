package com.example.addons.services.contracts;


import com.example.addons.models.User;

public interface EmailService {
    void sendMessage(String to, String subject, String text);
    void sendUserCreationVerificationCode(User user, int code) ;
}