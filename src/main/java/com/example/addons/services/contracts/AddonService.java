package com.example.addons.services.contracts;

import com.example.addons.models.Addon;

import java.util.List;

public interface AddonService {

    Addon getById(int userId);
    List<Addon> getAll();

    void create(Addon addon);

    void update(Addon updatedAddon);

    void addTag(int addonId, String tagName);

    void removeTag(int addonId, String tagName);
}
