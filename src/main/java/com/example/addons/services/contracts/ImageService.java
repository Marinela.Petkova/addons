package com.example.addons.services.contracts;

import com.example.addons.models.Image;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {

    Image getFile(int imageId);

    int saveFile(MultipartFile file);

    void update(MultipartFile file, int userId);

    void delete(Image image);
}
