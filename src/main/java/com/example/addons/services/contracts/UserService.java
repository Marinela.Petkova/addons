package com.example.addons.services.contracts;

import com.example.addons.models.User;

import java.util.List;

public interface UserService extends IUserService{
    User getByUsername(String username);
    User getById(int userId);
    List<User> getAll();
    void update(User user, int userId);
    void delete(User user, int userId);
    public void blockUser(User user, int userId);
    public void unblockUser(User user, int userId);
    public void promoteToAdmin(User user, int userId);

//    void uploadPhoto(MultipartFile photo, User user, String path) throws IOException;
//
//    void deletePhoto(User user);

}
