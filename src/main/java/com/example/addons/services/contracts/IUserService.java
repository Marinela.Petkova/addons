package com.example.addons.services.contracts;

import com.example.addons.models.User;

public interface IUserService {

        void create(User user);
        void activateAccount(int code);
        void resendActivationCode(String username);

        void sendActivationEmail(User user);


}
