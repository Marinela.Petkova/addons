package com.example.addons.services.contracts;

import com.example.addons.models.Tag;

import java.util.List;


public interface TagService {
    List<Tag> getAll();
    Tag getById(long id);
    Tag getByName(String name);
    void create(Tag tag);
    void delete(long id);
}
