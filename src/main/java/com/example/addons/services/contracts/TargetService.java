package com.example.addons.services.contracts;

import com.example.addons.models.Target;

import java.util.List;

public interface TargetService {

    List<Target> getAll();
    Target getById(int id);
    Target getByName(String name);
    void create(Target tag);

}
