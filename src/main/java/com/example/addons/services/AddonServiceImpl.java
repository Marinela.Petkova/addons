package com.example.addons.services;

import com.example.addons.exceptions.DuplicateEntityException;
import com.example.addons.exceptions.EntityNotFoundException;
import com.example.addons.models.Addon;
import com.example.addons.models.Tag;
import com.example.addons.repositories.AddonRepository;
import com.example.addons.repositories.TagRepository;
import com.example.addons.services.contracts.AddonService;
import com.example.addons.services.contracts.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddonServiceImpl implements AddonService {

    private final AddonRepository addonRepository;
    private final TagService tagService;

    private final TagRepository tagRepository;


    @Override
    public Addon getById(int addonId) {
        return addonRepository.getById(addonId);
    }

    @Override
    public List<Addon> getAll() {
        return addonRepository.findAll();
    }

    @Override
    public void create(Addon addon) {
        boolean addonExists = true;
        try {
            addonRepository.findByAddonName(addon.getAddonName());
        } catch (EntityNotFoundException e) {
            addonExists = false;
        }
        if (addonExists) {
            throw new DuplicateEntityException("Addon", "name", addon.getAddonName());
        }
        addonRepository.save(addon);
    }

    @Override
    public void update(Addon updatedAddon) {
        addonRepository.saveAndFlush(updatedAddon);
    }

    @Override
    public void addTag(int addonId, String tagName) {
        Addon addon = addonRepository.getById(addonId);
        Tag tagToAdd;
        try {
            tagToAdd = tagService.getByName(tagName);
        } catch (EntityNotFoundException e) {
            Tag tag = new Tag();
            tag.setName(tagName);
            tagRepository.save(new Tag());
            tagToAdd = tagService.getByName(tagName);
        }
        if (addon.getTags().stream().anyMatch(b -> b.getName().equals(tagName))) {
            throw new DuplicateEntityException("Tag", "tagName", tagName);
        }
        addon.getTags().add(tagToAdd);
        addonRepository.saveAndFlush(addon);
    }

    @Override
    public void removeTag(int addonId, String tagName) {
        Addon addon = addonRepository.getById(addonId);
        Tag tagToRemove = tagService.getByName(tagName);
        if (tagToRemove == null) {
            throw new EntityNotFoundException("Tag", "tagName", tagName);
        }
        addon.getTags().remove(tagToRemove);
        addonRepository.saveAndFlush(addon);
    }


}
