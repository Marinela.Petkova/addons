package com.example.addons.services;

import com.example.addons.exceptions.FileStorageException;
import com.example.addons.helper.ImageUtility;
import com.example.addons.models.Image;
import com.example.addons.models.User;
import com.example.addons.repositories.ImageRepository;
import com.example.addons.services.contracts.ImageService;
import com.example.addons.services.contracts.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.example.addons.helper.Constants.ErrorMessages.IMAGE_ERROR_MESSAGE;

@Service
public class ImageServiceImpl implements ImageService {


    private final ImageRepository imageRepository;
    private final UserService userService;

    public ImageServiceImpl(ImageRepository imageRepository, UserService userService) {
        this.imageRepository = imageRepository;
        this.userService = userService;
    }

    @Override
    public Image getFile(int imageId) {
        return imageRepository.getImageById(imageId);
    }

    @Transactional
    @Override
    public int saveFile(MultipartFile file) {


        try {
            Image image = new Image(file.getOriginalFilename(),
                    file.getContentType(), ImageUtility.compressImage(file.getBytes()));

            imageRepository.save(image);
            return image.getId();

        } catch (Exception e) {
            throw new FileStorageException(e.getMessage(), e);
        }
    }

    @Override
    public void update(MultipartFile file, int userId) {

        if (file.isEmpty() || file.getSize() > 1000000) {
            throw new FileStorageException(IMAGE_ERROR_MESSAGE);
        }

        try {
            User user = userService.getById(userId);
            Image oldImage = user.getImage();

            if (oldImage.getId() == 1) {

                int id = saveFile(file);
                user.setImage(getFile(id));
                userService.update(user, userId);
            } else {
                oldImage.setName(file.getOriginalFilename());
                oldImage.setType(file.getContentType());
                oldImage.setImage(ImageUtility.compressImage(file.getBytes()));
                imageRepository.saveAndFlush(oldImage);
            }
        } catch (IOException e) {
            throw new FileStorageException(e.getMessage(), e);
        }
    }


    @Override
    public void delete(Image image) {

        imageRepository.delete(image);
    }

}
