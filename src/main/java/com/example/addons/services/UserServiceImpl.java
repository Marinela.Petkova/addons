package com.example.addons.services;

import com.example.addons.exceptions.*;
import com.example.addons.helper.CheckHelperForUsername;
import com.example.addons.models.User;
import com.example.addons.repositories.ImageRepository;
import com.example.addons.repositories.UserRepository;
import com.example.addons.services.contracts.EmailService;
import com.example.addons.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import static com.example.addons.helper.Constants.ErrorMessages.*;
import static com.example.addons.helper.Constants.Rest.*;
import static com.example.addons.helper.Constants.UserConstants.DEFAULT_PHOTO_ID;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final ImageRepository imageRepository;

    private final CheckHelperForUsername helper;

    private final EmailService emailService;
    private final BCryptPasswordEncoder encoder;

    private final Map<Integer, String> userToActivate;
    private final Map<Integer, Timestamp> codeValidity;
    private final Random random;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ImageRepository imageRepository, CheckHelperForUsername helper,
                           EmailService emailService) {
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.helper = helper;
        this.emailService = emailService;
        encoder = new BCryptPasswordEncoder();
        random = new Random();
        codeValidity = new HashMap<>();
        userToActivate = new HashMap<>();
    }

    @Override
    public User getByUsername(String username) {

        return userRepository.findByUsername(username);
    }

    @Override
    public User getById(int userId) {
        try {
            User userToBeReturned = userRepository.getById(userId);
            if (userToBeReturned.isDeleted()) {
                throw new EntityNotFoundException(USER_NOT_FOUND);
            }
            return userToBeReturned;
        }catch (EntityNotFoundException e){
            throw new EntityNotFoundException(USER_NOT_FOUND);
        }


    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll().stream()
                .filter(User::isActivated)
                .filter(i -> !i.isDeleted())
                .collect(Collectors.toList());
    }

    @Override
    public void create(User user) {

        if (checkIfEmailExists(user)) {
            throw new DuplicateEntityException(DUPLICATE_EMAIL_ERROR_MESSAGE);
        }


        if (checkIfUsernameExists(user)) {
            throw new DuplicateEntityException(DUPLICATE_USERNAME_ERROR_MESSAGE);
        }
        if (checkIfPhoneNumberExists(user)) {
            throw new DuplicateEntityException(DUPLICATE_PHONE_NUMBER_ERROR_MESSAGE);
        }
        user.setPassword(encoder.encode(user.getPassword()));
        user.setActivated(false);
        user.setAdmin(false);
        user.setImage(imageRepository.getImageById(DEFAULT_PHOTO_ID));
        userRepository.save(user);

        sendActivationEmail(user);

    }

    private boolean checkIfUsernameExists(User user) {

        try {
            User byUsername = userRepository.findByUsername( user.getUsername());

            return byUsername.getId() != user.getId();

        } catch (EntityNotFoundException e) {
            return false;
        }

    }

    private boolean checkIfEmailExists(User user) {

        boolean emailExists = true;
        try {
            User userByEmail = userRepository.findByEmail(user.getEmail());
            if (user.getId() == userByEmail.getId()) {
                emailExists = false;
            }
        } catch (EntityNotFoundException e) {
            emailExists = false;
        }
        if (emailExists) {
            throw new DuplicateEntityException(DUPLICATE_EMAIL_ERROR_MESSAGE);
        }

        return false;

    }

    @Override
    public void update(User user, int userId) {

        if (!user.isAdmin() || user.getId() != userId) {
            throw new UnauthorizedOperationException(YOU_ARE_NOT_AUTHORIZED);
        }
        if (checkIfEmailExists(user)) {
            throw new DuplicateEntityException(DUPLICATE_EMAIL_ERROR_MESSAGE);
        }

        if (checkIfPhoneNumberExists(user)) {
            throw new DuplicateEntityException(DUPLICATE_PHONE_NUMBER_ERROR_MESSAGE);
        }
        userRepository.saveAndFlush(user);
    }

    @Override
    public void delete(User user, int userId) {

        if (!user.isAdmin() || user.getId() != userId) {
            throw new UnauthorizedOperationException(YOU_ARE_NOT_AUTHORIZED);
        }
        user.setDeleted(true);
        userRepository.saveAndFlush(user);
    }

    @Override
    public void activateAccount(int code) {
        if (!userToActivate.containsKey(code))
            throw new WrongActivationCodeException(ACTIVATION_CODE_NOT_ACTIVE);
        Timestamp now = Timestamp.from(Instant.now());
        if (codeValidity.get(code).before(now)) {
            userToActivate.remove(code);
            codeValidity.remove(code);
            throw new WrongActivationCodeException(ACTIVATION_CODE_IS_EXPIRED);
        }
        User user = userRepository.findByUsername(userToActivate.get(code));
        user.setActivated(true);
        userToActivate.remove(code);

        codeValidity.remove(code);
        userRepository.saveAndFlush(user);
    }

    @Override
    public void resendActivationCode(String username) {

        User user = userRepository.findByUsername(username);
        if (user.isActivated())
            throw new ForbiddenOperationException("User already activated!");
        sendActivationEmail(user);
    }

    @Override
    public void sendActivationEmail(User user) {
        String email = user.getEmail();
        int code = getActivationCode(user);
        emailService.sendMessage(email, "Account activation", String.valueOf(code));
        emailService.sendUserCreationVerificationCode(user, code);
        System.out.println(code);
    }

    private int getActivationCode(User user) {
        int code = getCode();
        if (userToActivate.containsKey(code))
            getActivationCode(user);
        userToActivate.put(code, user.getUsername());
        codeValidity.put(code, getActivationTime(5));
        return code;
    }

    private int getCode() {
        return 1000 + random.nextInt(2000);
    }

    private Timestamp getActivationTime(int minutes) {
        Timestamp out = Timestamp.from(Instant.now());
        out.setTime(out.getTime() + ((60 * minutes) * 1000));
        return out;
    }

    public void blockUser(User user, int userId) {
        try {
            User userToBeBlocked = getById(userId);

            if (!user.isAdmin() || user.getId() != userId) {
                throw new UnauthorizedOperationException(YOU_ARE_NOT_AUTHORIZED);
            }
            if (userToBeBlocked.isBlocked()) {
                throw new DuplicateEntityException(USER_IS_ALREADY_BLOCKED);
            }

            if (!user.isAdmin()) {
                throw new ForbiddenOperationException(ADMINS_CAN_NOT_BE_BLOCKED);
            }
            userToBeBlocked.setBlocked(true);
            userRepository.saveAndFlush(userToBeBlocked);

        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(USER_NOT_FOUND);
        }


    }

    public void unblockUser(User user, int userId) {

        try {
            User userToBeUnblocked = getById(userId);

            if (!user.isAdmin() || user.getId() != userId) {
                throw new UnauthorizedOperationException(YOU_ARE_NOT_AUTHORIZED);
            }
            if (!userToBeUnblocked.isBlocked()) {
                throw new DuplicateEntityException(USER_IS_NOT_BLOCKED);
            }

            userToBeUnblocked.setBlocked(false);
            userRepository.saveAndFlush(userToBeUnblocked);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(USER_NOT_FOUND);
        }

    }

    public void promoteToAdmin(User user, int userId) {

        try {
            User userToBePromoted = getById(userId);

            if (userToBePromoted.isAdmin()) {
                throw new DuplicateEntityException(USER_IS_ALREADY_ADMIN);
            }
            if(userToBePromoted.isBlocked() ||
                    userToBePromoted.isDeleted() ||
                    !userToBePromoted.isActivated()){
                throw new ForbiddenOperationException(NOT_ALLOWED_OPERATION);
            }
            if (!user.isAdmin()) {
                throw new UnauthorizedOperationException(YOU_ARE_NOT_AUTHORIZED);
            }

            userToBePromoted.setAdmin(true);
            userRepository.saveAndFlush(userToBePromoted);

        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(USER_NOT_FOUND);
        }

    }

    private boolean checkIfPhoneNumberExists(User user) {

        try {
            User byPhone = userRepository.findByPhoneNumber(user.getPhoneNumber());

            return byPhone.getId() != user.getId();

        } catch (EntityNotFoundException e) {
            return false;
        }

    }
//    @Override
//    @Transactional
//    public void uploadPhoto(MultipartFile photo, User user, String path) {
//        try {
//            Image newPhoto = handlePhotoUpload(photo, user, path);
//            imageRepository.create(newPhoto);
//            user.setImage(newPhoto);
//            userRepository.update(user);
//        } catch (IOException e) {
//            throw new FileStorageException(e.getMessage());
//        }
//    }
//
//    private Image handlePhotoUpload (MultipartFile multipartFile, User user, String path) throws IOException {
//        Image photo = Image.builder().build();
//        String fileName = user.getId() + ".pnj";
//        String savedPath = path + fileName;
//        if (user.getImage().getId() == DEFAULT_PHOTO_ID) {
//            // not sure
//            photo.setName(savedPath);
//        } else {
//            photo = user.getImage();
//        }
//        imageRepository.create(photo);
//        return photo;
//    }
//
//    @Override
//    @Transactional
//    public void deletePhoto(User user) {
//        if (user.getImage().getId() != DEFAULT_PHOTO_ID) {
//            Image oldPhoto = imageRepository.getImageById(user.getImage().getId());
//            user.setImage(imageRepository.getImageById(DEFAULT_PHOTO_ID));
//            imageRepository.delete(oldPhoto.getId());
//        }
//        userRepository.update(user);
//    }

}
