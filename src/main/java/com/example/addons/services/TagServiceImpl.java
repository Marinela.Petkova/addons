package com.example.addons.services;

import com.example.addons.exceptions.DuplicateEntityException;
import com.example.addons.exceptions.EntityNotFoundException;
import com.example.addons.models.Tag;
import com.example.addons.repositories.TagRepository;
import com.example.addons.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.addons.helper.Constants.QueryConstants.ADDON;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.findAll();
    }

    @Override
    public Tag getById(long id) {
        return tagRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(ADDON, id));
    }

    @Override
    public Tag getByName(String name) {
        return tagRepository.findByName(name);
    }

    @Override
    public void create(Tag tag) {
        boolean tagNameExists = true;

        try {
            tagRepository.findByName(tag.getName());
        } catch (EntityNotFoundException e) {
            tagNameExists = false;
        }

        if (tagNameExists) {
            throw new DuplicateEntityException("Tag", "name", tag.getName());
        }

        tagRepository.save(tag);
    }

    @Override
    public void delete(long id) {
        tagRepository.deleteTagById(id);
    }
}

