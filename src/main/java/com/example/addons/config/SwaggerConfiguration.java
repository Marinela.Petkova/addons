package com.example.addons.config;

import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

public class SwaggerConfiguration {

    private static final String API_EMAIL = "spring.mail.username";

    private final String apiEmail;

    public SwaggerConfiguration(Environment environment) {
        this.apiEmail = environment.getProperty(API_EMAIL);
    }

    @Bean
    public Docket swaggerConfigurationDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .apis(RequestHandlerSelectors.basePackage("com.example.addons"))
                .build()
                .apiInfo(getApiInfo());

    }


    private ApiInfo getApiInfo() {
        String title = "Addons API Documentation";
        String description = "Java Spring Boot Application, where the goal is to have a place" +
                " that makes it easy for a user to find the right tool for their needs";
        String version = "1.0";
        Contact ivan_tsvetanov_and_marinela_petkova = getContact();

        return new ApiInfo(
                title,
                description,
                version,
                "https://gitlab.com/Marinela.Petkova/addons",
                ivan_tsvetanov_and_marinela_petkova,
                "API License",
                "https://gitlab.com/Marinela.Petkova/addons",
                Collections.emptyList());

    }

    private Contact getContact() {
        String names = "Ivan Tsvetanov and Marinela Petkova";
        String repoUrl = "https://gitlab.com/Marinela.Petkova/addons";
        return new Contact(names,
                repoUrl,
                apiEmail);
    }
}
