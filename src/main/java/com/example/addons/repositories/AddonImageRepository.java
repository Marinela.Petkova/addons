package com.example.addons.repositories;

import com.example.addons.models.AddonImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddonImageRepository extends JpaRepository<AddonImage, Integer> {

    AddonImage getImageById(int image_id);

}
