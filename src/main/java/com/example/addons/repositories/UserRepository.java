package com.example.addons.repositories;

import com.example.addons.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByEmail(String email);

    User findByUsername(String username);

    User findByPhoneNumber(String phoneNumber);

}
