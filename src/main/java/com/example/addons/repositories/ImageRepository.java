package com.example.addons.repositories;

import com.example.addons.models.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Integer> {


    Image getImageById(int image_id);

}

