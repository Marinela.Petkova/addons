package com.example.addons.repositories;

import com.example.addons.models.Addon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface AddonRepository extends JpaRepository<Addon, Integer> {

    void findByAddonName(String addonName);
}
