package com.example.addons.repositories;

import com.example.addons.models.Target;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TargetRepository extends JpaRepository<Target, Integer> {

    Target findByTargetName(String targetName);
}
