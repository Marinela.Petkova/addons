package com.example.addons.repositories;

import com.example.addons.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface TagRepository extends JpaRepository<Tag, Long> {


    Tag findByName(String name);

    void deleteTagById(long id);

}
