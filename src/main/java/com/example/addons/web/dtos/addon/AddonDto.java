package com.example.addons.web.dtos.addon;

import com.example.addons.models.Tag;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;

import static com.example.addons.helper.Constants.QueryConstants.BINARY_CONTENT;
import static com.example.addons.helper.Constants.QueryConstants.ORIGIN;
import static com.example.addons.helper.Constants.ValidationConstants.*;

@Data
@Component
@Getter
@Setter
public class AddonDto {

    @NotNull(message = "Addon's name can't be empty")
    @Size(min = MIN_ADDON_NAME_LENGTH, max = MAX_ADDON_NAME_LENGTH,
            message = "Addon's name should be between 3 and 30 symbols")
    private String name;

    private String targetIDE;

    @NotNull(message = "Tag name can't be empty")
    @Size(min = MIN_DESCRIPTION_LENGTH, max = MAX_DESCRIPTION_LENGTH,
            message = "Description should be between 5 and 50 symbols")
    private String description;

    @NotNull
    @CreationTimestamp
    private LocalDateTime creationTime;

    @Positive
    private int downloads;

    private Set<Tag> tags;

    @NotBlank(message = ORIGIN + CANT_BE_EMPTY)
    @Length(min = MIN_ORIGIN_LENGTH, max = MAX_ORIGIN_LENGTH)
    private String origin;

    @NotBlank(message = BINARY_CONTENT + CANT_BE_EMPTY)
    @Length(min = MIN_BINARY_CONTENT_LENGTH, max = MAX_BINARY_CONTENT_LENGTH)
    private String binaryContent;
}

