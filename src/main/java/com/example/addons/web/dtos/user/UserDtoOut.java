package com.example.addons.web.dtos.user;

import com.example.addons.models.Image;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class UserDtoOut {


    private String firstName;

    private String lastName;

    private String email;

    private String username;

    private Image image;

    private String phone;

}
