package com.example.addons.web.dtos.user;

import com.example.addons.validations.PasswordMatches;
import com.example.addons.validations.impl.Passwordable;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;

import javax.validation.constraints.*;

import static com.example.addons.helper.Constants.QueryConstants.*;
import static com.example.addons.helper.Constants.ValidationConstants.*;

@Data
@Component
@Getter
@Setter
@PasswordMatches(message = PLEASE_PROVIDE_MATCHING_PASSWORDS)
public class UserDto implements Passwordable {

    @NotNull(message = FIRST_NAME + CANT_BE_EMPTY)
    @Length(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
    private String firstName;

    @NotNull(message = LAST_NAME + CANT_BE_EMPTY)
    @Length(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
    private String lastName;

    @NotNull(message = USERNAME + CANT_BE_EMPTY)
    @Length(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
    private String username;

    @NotNull(message = THIS_FIELD_CANNOT_BE_BLANK)
    @Pattern(regexp = PASSWORD_REGEX,
            message = PASSWORD_VALIDATION)
    private String password;

    @NotEmpty(message = PLEASE_PROVIDE_MATCHING_PASSWORDS)
    private String confirmationPassword;

    @NotBlank(message = THIS_FIELD_CANNOT_BE_BLANK)
    @Email(regexp = EMAIL_REGEX)
    private String email;

    @NotBlank(message = THIS_FIELD_CANNOT_BE_BLANK)
    @Digits(message = PHONE_NUMBER_LENGTH, fraction = 0, integer = 10)
    private String phoneNumber;

}
