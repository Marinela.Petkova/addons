package com.example.addons.web.mappers;

import com.example.addons.exceptions.EntityNotFoundException;
import com.example.addons.models.Addon;
import com.example.addons.models.Target;
import com.example.addons.models.User;
import com.example.addons.services.contracts.AddonImageService;
import com.example.addons.services.contracts.TargetService;
import com.example.addons.web.dtos.addon.AddonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddonMapper {

    private final AddonImageService addonImageService;
    private final TargetService targetService;

    @Autowired
    public AddonMapper(AddonImageService addonImageService,
                       TargetService targetService) {
        this.addonImageService = addonImageService;
        this.targetService = targetService;
    }

    public Addon addonDtoToAddon(AddonDto addonDto, User user) {
        Addon addon = new Addon();
        addon.setAddonName(addonDto.getName());
        addon.setTags(addonDto.getTags());
        addon.setDescription(addonDto.getDescription());
        addon.setDownloads(addonDto.getDownloads());
        Target target;
        try {
            target = targetService.getByName(addonDto.getTargetIDE());
        } catch (EntityNotFoundException e) {
            targetService.create(new Target(addonDto.getTargetIDE()));
            target = targetService.getByName(addonDto.getTargetIDE());
        }
        addon.setTarget(target);
        addon.setAddonImage(addonImageService.getFile(2));
        addon.setBinaryContent(addonDto.getBinaryContent());
        addon.setCreator(user);
        addon.setCreationTime(addonDto.getCreationTime());
        addon.setOrigin(addonDto.getOrigin());
        addon.setApproved(false);
        addon.setFeatured(false);
        return addon;
    }

//    public Addon addonDtoToAddon(AddonDto addonDto, User user, int id) {
//        Addon addon = addonDtoToAddon(addonDto, user);
//        addon.setId(id);
//        return addon;
//    }
}

