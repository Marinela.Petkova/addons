package com.example.addons.web.mappers;

import com.example.addons.helper.ImageUtility;
import com.example.addons.models.Image;
import com.example.addons.models.User;
import com.example.addons.services.contracts.ImageService;
import com.example.addons.web.dtos.user.UserDto;
import com.example.addons.web.dtos.user.UserDtoOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class UserMapper {

    private final ImageService imageService;


    @Autowired
    public UserMapper(ImageService imageService) {
        this.imageService = imageService;
    }


    public UserDtoOut userToUserDtoOut(User user) {
        UserDtoOut userDtoOut = new UserDtoOut();
        userDtoOut.setFirstName(user.getFirstName());
        userDtoOut.setLastName(user.getLastName());
        userDtoOut.setUsername(user.getUsername());
        userDtoOut.setEmail(user.getEmail());
        userDtoOut.setPhone(user.getPhoneNumber());
        Image decompressedImage = new Image();
        decompressedImage.setId(user.getImage().getId());
        decompressedImage.setName(user.getImage().getName());
        decompressedImage.setType(user.getImage().getType());
        decompressedImage.setImage(ImageUtility.decompressImage(user.getImage().getImage()));
        userDtoOut.setImage(decompressedImage);

        return userDtoOut;
    }

    public User userDtoToUser(UserDto dto) {

        if (!dto.getPassword().equals(dto.getConfirmationPassword())) {
            throw new UnsupportedOperationException("Password confirmation should match password.");
        }
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setPhoneNumber(dto.getPhoneNumber());
        user.setDate(LocalDateTime.now());
//        user.setActivated(false);
//        user.setAdmin(false);
//        user.setDeleted(false);
//        user.setBlocked(false);

 //      user.setImage(imageService.getFile(1));

        return user;
    }

}
