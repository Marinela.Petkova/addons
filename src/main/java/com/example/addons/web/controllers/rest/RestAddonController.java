package com.example.addons.web.controllers.rest;

import com.example.addons.exceptions.DuplicateEntityException;
import com.example.addons.exceptions.EntityNotFoundException;
import com.example.addons.exceptions.FileStorageException;
import com.example.addons.helper.ImageUtility;
import com.example.addons.models.Addon;
import com.example.addons.models.AddonImage;
import com.example.addons.models.User;
import com.example.addons.services.contracts.AddonImageService;
import com.example.addons.services.contracts.AddonService;
import com.example.addons.services.contracts.UserService;
import com.example.addons.web.dtos.addon.AddonDto;
import com.example.addons.web.mappers.AddonMapper;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/addons")
public class RestAddonController {

    private final AddonService addonService;
    private final UserService userService;
    private final AddonMapper addonMapper;

    private final AddonImageService addonImageService;

    @Autowired
    public RestAddonController(AddonService addonService,
                               UserService userService,
                               AddonMapper addonMapper,
                               AddonImageService addonImageService) {
        this.addonService = addonService;
        this.userService = userService;
        this.addonMapper = addonMapper;
        this.addonImageService = addonImageService;
    }

    @GetMapping
    public List<Addon> getAll() {
        try {
            return addonService.getAll();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Addon getById(@ApiParam(value = "Valid ID for the Addon you want to retrieve", example = "1")
                         @PathVariable Integer id) {
        try {
            return addonService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void create(@Validated @RequestBody AddonDto addonDto, Principal principal) {
        try {
            User user = userService.getByUsername(principal.getName());
            Addon addon = addonMapper.addonDtoToAddon(addonDto, user);
            addonService.create(addon);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@PathVariable int id, @Validated @RequestBody AddonDto addonDto, Principal principal) {
        try {
            User user = userService.getByUsername(principal.getName());
            Addon addonToUpdate = addonService.getById(id);
            if (!addonToUpdate.getCreator().equals(user) || !user.isAdmin()){
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not allowed!");
            }
            Addon updatedAddon = addonMapper.addonDtoToAddon(addonDto, addonToUpdate.getCreator());
            addonService.update(updatedAddon);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/feature")
    public void featureAddon(@PathVariable int id, Principal principal){
        try {
            User user = userService.getByUsername(principal.getName());
            Addon addon = addonService.getById(id);
            if (!user.isAdmin()){
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not allowed!");
            }
            addon.setFeatured(true);
            addonService.update(addon);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/approve")
    public void approveAddon(@PathVariable int id, Principal principal){
        try {
            User user = userService.getByUsername(principal.getName());
            Addon addon = addonService.getById(id);
            if (!user.isAdmin()){
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not allowed!");
            }
            addon.setApproved(true);
            addonService.update(addon);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/un_feature")
    public void removeFromFeatured(@PathVariable int id, Principal principal){
        try {
            User user = userService.getByUsername(principal.getName());
            Addon addon = addonService.getById(id);
            if (!user.isAdmin()){
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not allowed!");
            }
            addon.setFeatured(false);
            addonService.update(addon);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/add-tag/{addonId}")
    public void addTag(
            @PathVariable int addonId, @RequestParam String tagName) {
        try {
            addonService.addTag(addonId, tagName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/remove-tag/{addonId}")
    public void removeTag(
            @PathVariable int addonId, @RequestParam String tagName) {
        try {
            addonService.removeTag(addonId, tagName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("image/{addon_id}")
    public ResponseEntity<byte[]> getImage(
            @PathVariable("addon_id") int addonId) {

        try {
            final AddonImage dbImage = addonService.getById(addonId).getAddonImage();

            return ResponseEntity
                    .ok()
                    .contentType(MediaType.valueOf(dbImage.getType()))
                    .body(ImageUtility.decompressImage(dbImage.getImage()));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("image/")
    public ResponseEntity<ImageUploadResponse> uploadNewImage(
            @RequestParam("image") MultipartFile file) {

        try {
            addonImageService.saveFile(file);

            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ImageUploadResponse("Image uploaded successfully: " +
                            file.getOriginalFilename()));
        } catch (FileStorageException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }

    @PutMapping("image/{addon_id}")
    public ResponseEntity<ImageUploadResponse> update(
            @PathVariable("addon_id") int addonId,
            @RequestParam("image") MultipartFile file) {
        try {
            addonImageService.update(file, addonId);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ImageUploadResponse("Image updated successfully: " +
                            file.getOriginalFilename()));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (FileStorageException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }

    @DeleteMapping("image/{addon_id}")
    public ResponseEntity<ImageUploadResponse> deleteImage(
            @PathVariable("addon_id") int addonId) {
        try {
            final AddonImage dbImage = addonService.getById(addonId).getAddonImage();
            if (dbImage.getId() == 2) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ImageUploadResponse("Default image can not be deleted: " + dbImage.getName()));
            } else {
                Addon addon = addonService.getById(addonId);
                addon.setAddonImage(addonImageService.getFile(2));
                addonService.update(addon);
                addonImageService.delete(dbImage);
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ImageUploadResponse("Image deleted successfully: " +
                                dbImage.getName()));
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
