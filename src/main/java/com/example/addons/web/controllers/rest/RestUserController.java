package com.example.addons.web.controllers.rest;

import com.example.addons.exceptions.*;
import com.example.addons.helper.CheckHelperForUsername;
import com.example.addons.models.Image;
import com.example.addons.models.User;
import com.example.addons.services.UserServiceImpl;
import com.example.addons.services.contracts.ImageService;
import com.example.addons.web.dtos.user.UserDto;
import com.example.addons.web.dtos.user.UserDtoOut;
import com.example.addons.web.mappers.UserMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.addons.helper.Constants.QueryConstants.PHOTO;
import static com.example.addons.helper.Constants.Rest.*;

@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping("/api/users")
public class RestUserController {

    private final UserServiceImpl userService;

    private final ImageService imageService;

    private final UserMapper userMapper;

    private final CheckHelperForUsername helper;



    @GetMapping("/{id}")
    @ApiOperation(value = "Show User by Id", notes = "Returns information for user by Id.",
            response = UserDtoOut.class)
    public UserDtoOut getById(@ApiParam(value = "Valid ID for the User you want to retrieve", example = "1")
                              @PathVariable Integer id) {
        try {
            User user = userService.getById(id);
            return userMapper.userToUserDtoOut(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    @ApiOperation(value = "Show All users", notes = "Returns all the information for all users",
            response = UserDtoOut.class)
    public List<UserDtoOut> getAll() {
        List<User> users = userService.getAll();
        return users.stream()
                .map(userMapper::userToUserDtoOut).collect(Collectors.toList());
    }


    @PostMapping
    @ApiOperation(value = "Register new User .", notes = "Create new inactive user." +
            " Activate with email within 24 hours of registering.",
            response = User.class)
    @CrossOrigin(origins = "*")
    public User register(@Valid @RequestBody UserDto registerDto) {
        try {
            User newUser = userMapper.userDtoToUser(registerDto);
            userService.create(newUser);
            return newUser;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/activate/{code}")
    public String activateUser(@PathVariable int code) {
        try {
            userService.activateAccount(code);
            return "Account activated";
        } catch (WrongActivationCodeException e) {
            return "Code has expired! Resend code.";
        } catch (ForbiddenOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PostMapping("/activate/new-code")
    public String sendNewActivationCode(Principal principal) {
        try {
            userService.resendActivationCode(principal.getName());
            return "New activation code sent";
        } catch (ForbiddenOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public void updateUser(@PathVariable int id, Principal principal,
                           @Valid @RequestBody UserDto userDto) {

        try {
            User user = helper.getUser(principal);

            User userToBeUpdated = userMapper.userDtoToUser(userDto);
            userToBeUpdated.setId(id);
            userService.update(user, id);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/block")
    public void blockUser(@PathVariable int id, Principal principal) {
        try {
            User user = helper.getUser(principal);
            userService.blockUser(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @PutMapping("/unblock")
    public void unblockUser(@PathVariable int id, Principal principal) {
        try {
            User user = helper.getUser(principal);
            userService.unblockUser(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @PutMapping("/promote_admin")
    private void promoteToAdmin(@PathVariable int userId, Principal principal) {
        try {
            User user = helper.getUser(principal);
            userService.promoteToAdmin(user, userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/deletion")
    public void requestUserDelete(@PathVariable int id,
                                  HttpServletRequest httpServlet, Principal principal) {
        try {
            User user = helper.getUser(principal);
            userService.delete(user, id);
            httpServlet.logout();

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    @PostMapping("/photo")
    public ResponseEntity<String> uploadPhoto(Principal principal,
                                              @RequestParam(value = PHOTO, required = false) MultipartFile photo) {
        try {
            User user = helper.getUser(principal);
            imageService.saveFile(photo);
            imageService.update(photo, user.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.ok().body(FILE_RECEIVED_SUCCESSFULLY);
    }

    @DeleteMapping("/photo")
    public ResponseEntity<String> deletePhoto(Principal principal) {
        try {

            final Image dbImage = helper.getUser(principal).getImage();
            if (dbImage.getId() == 1) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, CAN_NOT_BE_DELETED);
            } else {
                User user = helper.getUser(principal);
                user.setImage(imageService.getFile(1));
                userService.update(user, user.getId());
                imageService.delete(dbImage);
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return ResponseEntity.ok().body(FILE_DELETED_SUCCESSFULLY);
    }


    // filter ToDo !!!!!!!!!!!
}
