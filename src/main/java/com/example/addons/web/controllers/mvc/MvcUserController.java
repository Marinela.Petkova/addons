package com.example.addons.web.controllers.mvc;

import com.example.addons.exceptions.DuplicateEntityException;
import com.example.addons.helper.Constants;
import com.example.addons.services.UserServiceImpl;
import com.example.addons.web.dtos.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static com.example.addons.helper.Constants.UserConstants.CREATE_USER_DTO;
import static com.example.addons.helper.Constants.ValidationConstants.*;
import static com.example.addons.helper.Constants.Views.EMAIL_CONFIRMATION_VIEW;
import static com.example.addons.helper.Constants.Views.REGISTER_VIEW;

@Controller
@RequestMapping("/users")
public class MvcUserController {

    private final UserServiceImpl userService;

    @Autowired
    public MvcUserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping("/user/registration")
    public String registerUserAccount(
            @ModelAttribute("user")
            @Valid UserDto userDto,
            BindingResult bindingResult,
            HttpServletRequest request,
            Errors errors,
            Model model) {

        if (bindingResult.hasErrors()) {
            prepareRegistrationPage(model, userDto, request);
            return REGISTER_VIEW;
        }

        try {

            // ToDo!!!!!!!!!!!!!!!!!!!
          //  userService.registerNewUserAccount(userDto);
            clearSession(request);
            return EMAIL_CONFIRMATION_VIEW;
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue(Constants.QueryConstants.EMAIL, ERROR +
                    CREATE_USER_DTO, e.getMessage());
            return handleError(userDto, model, BINDING_RESULT_ERRORS, e.getMessage());
        }
    }

    private String handleError(UserDto dto, Model model, String errorName, String message) {
        model.addAttribute(CREATE_USER_DTO, dto);
        model.addAttribute(errorName, message);

        return "to be continued :)";
        //  return REGISTER_VIEW;
    }

    private void clearSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute(REGISTER_FIRST);
        }
    }

    private void prepareRegistrationPage(Model model,
                                         UserDto dto,
                                         HttpServletRequest request) {
        String msg = getRegisterFirstMessage(request);
        model.addAttribute(REGISTER_FIRST, msg);
        model.addAttribute(CREATE_USER_DTO, dto);
    }

    @Nullable
    private String getRegisterFirstMessage(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        String attribute = null;
        if (session != null) {
            attribute = (String) session.getAttribute(REGISTER_FIRST);
        }
        return attribute;
    }

}
