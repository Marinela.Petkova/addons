package com.example.addons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class AddonsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AddonsApplication.class, args);
    }

}
