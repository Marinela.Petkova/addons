package com.example.addons.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="images")
public class Image extends BaseEntity {


    private String name;

    private String type;

    @Column( unique = false, nullable = false, length = 1000000)
    private byte[] image;

    @JsonIgnore
    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "image")
    @JsonIgnoreProperties
    private Set<User> listOfUsers;

    public Image(String originalFilename, String contentType, byte[] compressImage) {
        super();
    }
}



