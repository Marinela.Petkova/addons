package com.example.addons.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Set;

import static com.example.addons.helper.Constants.ErrorMessages.ERROR_MESSAGE_TAG_NAME_LENGTH;
import static com.example.addons.helper.Constants.ValidationConstants.MAX_TAG_NAME_LENGTH;
import static com.example.addons.helper.Constants.ValidationConstants.MIN_TAG_NAME_LENGTH;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "tags")
public class Tag extends BaseEntity {


    @Size(min = MIN_TAG_NAME_LENGTH, max = MAX_TAG_NAME_LENGTH,
            message = ERROR_MESSAGE_TAG_NAME_LENGTH)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<Addon> addons;

}
