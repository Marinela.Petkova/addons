package com.example.addons.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.Set;

import static com.example.addons.helper.Constants.QueryConstants.*;
import static com.example.addons.helper.Constants.ValidationConstants.*;


@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User extends BaseEntity {

    @NotBlank(message = USERNAME + CANT_BE_EMPTY)
    @Length(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
    @Column(unique = true)
    private String username;

    private String password;


    @NotBlank(message = THIS_FIELD_CANNOT_BE_BLANK)
    @Email(regexp = EMAIL_REGEX)
    @Column(unique = true)
    private String email;

    @NotBlank(message = PASSWORD_VALIDATION)
    @Column(unique = true)
    private String phoneNumber;


    @ManyToOne
    @Fetch(FetchMode.JOIN)
    private Image image;

    @CreationTimestamp
    private LocalDateTime date;

   @NotBlank(message = FIRST_NAME + CANT_BE_EMPTY)
    @Length(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
    private String firstName;

    @NotBlank(message = LAST_NAME + CANT_BE_EMPTY)
    @Length(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
    private String lastName;

    @JsonIgnore
    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "creator")
    @JsonIgnoreProperties
    private Set<Addon> listOfAddons;

  //  @NotEmpty
    private boolean isAdmin;

   // @NotEmpty
    private boolean isActivated;

 //   @NotEmpty
    private boolean isDeleted;

  //  @NotEmpty
    private boolean isBlocked;


    //    Check
    public String getRole() {
        if (isAdmin) {
            return "ADMIN";
        } else {
            return "USER";
        }
    }
}
