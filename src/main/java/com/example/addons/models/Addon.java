package com.example.addons.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.Set;

import static com.example.addons.helper.Constants.QueryConstants.*;
import static com.example.addons.helper.Constants.ValidationConstants.*;


@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "addons")
public class Addon extends BaseEntity {


    @ManyToOne
    private User creator;

    @Length(min = MIN_ADDON_NAME_LENGTH, max = MAX_ADDON_NAME_LENGTH)
    @Column(unique = true)
    private String addonName;

    @NotBlank(message = DESCRIPTION + CANT_BE_EMPTY)
    @Length(min = MIN_DESCRIPTION_LENGTH, max = MAX_DESCRIPTION_LENGTH)
    @Column(nullable = false)
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    private Target target;

    @NotBlank(message =BINARY_CONTENT +  CANT_BE_EMPTY)
    @Length(min = MIN_BINARY_CONTENT_LENGTH, max = MAX_BINARY_CONTENT_LENGTH)
    private String binaryContent;


    @NotBlank(message = ORIGIN + CANT_BE_EMPTY)
    @Length(min = MIN_ORIGIN_LENGTH, max = MAX_ORIGIN_LENGTH)
    private String origin;

    @OneToOne(cascade = CascadeType.REMOVE)
    private AddonImage addonImage;

    @NotNull
    @CreationTimestamp
    private LocalDateTime creationTime;

    @Positive
    private int downloads;

    @NotNull
    private boolean isFeatured;

    @NotNull
    private boolean isApproved;


    @JsonIgnoreProperties("addons")
    @ManyToMany(mappedBy = "addons", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Tag> tags;


}

