package com.example.addons.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="addon_images")
public class AddonImage extends BaseEntity{


    private String name;


    private String type;

    @Column(unique = false, nullable = false, length = 1000000)
    private byte[] image;

}
