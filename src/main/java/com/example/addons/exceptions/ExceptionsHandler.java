package com.example.addons.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> handleException(EntityNotFoundException exception) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("status", HttpStatus.NOT_FOUND);
        body.put("message", exception.getMessage());
        body.put("timestamp", Calendar.getInstance().getTime());
        return new ResponseEntity<>(
                body,
                HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(DuplicateEntityException.class)
    public ResponseEntity<?> handleException(DuplicateEntityException exception) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("status", HttpStatus.CONFLICT);
        body.put("message", exception.getMessage());
        body.put("timestamp", Calendar.getInstance().getTime());
        return new ResponseEntity<>(
                body,
                HttpStatus.CONFLICT
        );
    }

    @ExceptionHandler(UnauthorizedOperationException.class)
    public ResponseEntity<?> handleException(UnauthorizedOperationException exception) {
        return getResponseEntity(exception.getMessage(), exception);
    }


    @ExceptionHandler(WrongActivationCodeException.class)
    public ResponseEntity<?> handleException(WrongActivationCodeException exception) {
        return getResponseEntity(exception.getMessage(), exception);
    }

    @ExceptionHandler(ForbiddenOperationException.class)
    public ResponseEntity<?> handleException(ForbiddenOperationException exception) {
        return getResponseEntity(exception.getMessage(), exception);
    }

    private ResponseEntity<?> getResponseEntity(String message, RuntimeException exception) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("status", HttpStatus.FORBIDDEN);
        body.put("message", message);
        body.put("timestamp", Calendar.getInstance().getTime());
        return new ResponseEntity<>(
                body,
                HttpStatus.FORBIDDEN
        );
    }


}
