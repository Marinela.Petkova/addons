package com.example.addons.exceptions;

public class WrongActivationCodeException extends RuntimeException{

    public WrongActivationCodeException(String message) {
        super(message);
    }
}
