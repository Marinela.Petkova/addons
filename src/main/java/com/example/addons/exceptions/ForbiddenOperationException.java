package com.example.addons.exceptions;

public class ForbiddenOperationException extends RuntimeException{

    public ForbiddenOperationException(String message) {
        super(message);
    }
}
